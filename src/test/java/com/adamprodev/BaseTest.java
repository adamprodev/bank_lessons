package com.adamprodev;

import io.restassured.RestAssured;
import org.junit.Before;

public class BaseTest {

    @Before
    public void setup() {
        RestAssured.baseURI = "https://api.github.com";
        RestAssured.port = 443;
    }
}
