package com.adamprodev;

import io.restassured.RestAssured;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import java.io.IOException;

public class MainTest extends BaseTest {

    @Test
    public void test() {

        RestAssured
                .given().log().all()
                .when()
                .request("GET", "/users/eugenp")
                .then().statusCode(200);

//        RestAssured
//                .get("/odd").then()
//                .assertThat().body("odd.ck", IsEqual.equalTo(12.2));
    }

    @Test
    public void givenUserDoesNotExists_whenUserInfoIsRetrieved_then404IsReceived()
            throws ClientProtocolException, IOException {

        // Given
        String name = RandomStringUtils.randomAlphabetic( 8 );
        System.out.println(name);
        HttpUriRequest request = new HttpGet( "https://api.github.com/users/" + name );

        // When
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

        // Then
        MatcherAssert.assertThat(
                httpResponse.getStatusLine().getStatusCode(),
                IsEqual.equalTo(HttpStatus.SC_NOT_FOUND));
    }
}
